using System;
using System.Reflection;
using Application.Cellulant.Queries;
using Application.Common.Interfaces;
using Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.VisualStudio.Web.CodeGeneration.Design;
using POSMiddlewareMerchant.Common;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
var configuration = builder.Services.BuildServiceProvider()
                        .GetRequiredService<IConfiguration>();

builder.Services.AddControllers();
builder.Services.AddInfrastructure(configuration);

var assembly = AppDomain.CurrentDomain.Load("Application");
builder.Services.AddMediatR(assembly);
builder.Services.AddAutoMapper(assembly);

builder.Services.AddScoped<IConfigurationManager, CustomConfigurationManager>();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.MapDefaultControllerRoute();

app.Run();