using System.Threading.Tasks;
using Application.Common.Interfaces;
using Microsoft.Extensions.Configuration;

namespace POSMiddlewareMerchant.Common
{
    public class CustomConfigurationManager : IConfigurationManager
    {
        private readonly IConfiguration _configuration;

        public CustomConfigurationManager(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public string RetrieveAppSettingsValue(string name)
        {
            return _configuration[name];
        }
    }
}