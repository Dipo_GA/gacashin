using System.Threading.Tasks;
using Application.Cellulant.Commands;
using Application.Cellulant.Commands.DTOs;
using Application.Cellulant.Queries;
using Application.Cellulant.Queries.DTOs;
using Microsoft.AspNetCore.Mvc;

namespace POSMiddlewareMerchant.Controllers.V1
{
    public class CellulantControllers : ApiControllerBase
    {
        // GET
        [HttpPost, Route("api/v1/GetCustomerInfo")]
        public async Task<ActionResult<CustomerInformationResponse>> GetCustomerInformation([FromBody]FetchCustomerInformationQuery query)
        {
            return await Mediator.Send(query);
        }
        
        [HttpPost, Route("api/v1/GetBankList")]
        public async Task<ActionResult<BankListResponse>> GetBankList([FromBody]FetchBankListQuery query)
        {
            return await Mediator.Send(query);
        }
        
        [HttpPost, Route("api/v1/RetrieveBeneficiaryNameEnquiry")]
        public async Task<ActionResult<NameEnquiryResponse>> RetrieveBeneficiaryNameEnquiry([FromBody]FetchNameEnquiryQuery query)
        {
            return await Mediator.Send(query);
        }
        
        [HttpPost, Route("api/v1/PostTransaction")]
        public async Task<ActionResult<PostTransactionResponse>> PostTransaction([FromBody]PostTransactionsCommand post)
        {
            return await Mediator.Send(post);
        }
    }
}