using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Domain.Entities.Cellulant;
using Infrastructure.Services;
using Moq;
using Xunit;

namespace POSMiddleWareMerchant.Test
{
    public class CustomAPICallsTest
    {
        private const string baseNameUrl = "SmartNameEnquiry/SmartWallet/Proxy";
        private const string baseUrl = "/SmartWallet/Proxy";
        
        private string customerinfourl = $"{baseUrl}/Login?&SourcePhone=2348083939479&Pin=1987&Channel=10";
        private string bankListUrl = $"{baseUrl}/DataOps?&Type=nip2banklist&Phone=234806050";
        private string nameEnquiry = $"{baseNameUrl}/NameEnquiry?AccountNo=0165637119&BankCode=000013";
        
        private readonly Mock<IHttpClientFactory> _httpFactory;
        
        private readonly CustomAPiCalls<CustomerInformation> _customAPICalls;
        private readonly CustomAPiCalls<BankList> _bankCustomAPICalls;
        private readonly CustomAPiCalls<string> _nameEnquiry;


        public CustomAPICallsTest()
        {
            _httpFactory = new Mock<IHttpClientFactory>();
            _httpFactory.Setup(x => x.CreateClient("CellulantBase")).Returns(new HttpClient()
            {
                BaseAddress = new Uri("http://41.73.252.230:28080/")
            });

            _bankCustomAPICalls = new CustomAPiCalls<BankList>(_httpFactory.Object);
            _customAPICalls = new CustomAPiCalls<CustomerInformation>(_httpFactory.Object);
            _nameEnquiry = new CustomAPiCalls<string>(_httpFactory.Object);
        }
        
        [Fact]
        public async Task Get_GivenUrl_ReturnsValidCustomerInformation()
        {
            var response = await _customAPICalls.Get(customerinfourl);
            
            Assert.Equal("00", response?.ResponseCode);
        }
        
        [Fact]
        public async Task Get_GivenUrl_ReturnsBankList()
        {
            var response = await _bankCustomAPICalls.GetAll(bankListUrl);
            
            Assert.Equal("000001", response.FirstOrDefault(x => x.BankCode == "000001").BankCode);
        }
        
        [Fact]
        public async Task Get_GivenUrl_ReturnsValidNameEnquiryResponseCode()
        {
            var response = await _nameEnquiry.GetString(nameEnquiry);
            var responseCode = response?.Split('|')[0];
            
            Assert.Equal("00", responseCode);
        }
    }
}