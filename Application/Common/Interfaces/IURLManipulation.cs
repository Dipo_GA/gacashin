using Application.Cellulant.Queries;

namespace Application.Common.Interfaces
{
    public interface IURLManipulation
    {
        string ResolveCustomerInformationURL(FetchCustomerInformationQuery reqObj, string url);
    }
}