using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Common.Interfaces
{
    public interface IAPICalls<T> where T: class
    {
        Task<T?> Get(string url);
        Task<IEnumerable<T>?> GetAll(string url);
        Task<string> GetString(string url);
        Task<T> QueryPost(string url);
        Task<T?> BodyPost(string url, T postObject);
    }
}