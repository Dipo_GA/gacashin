using System.Threading.Tasks;

namespace Application.Common.Interfaces
{
    public interface IConfigurationManager
    {
        string RetrieveAppSettingsValue(string name);
    }
}