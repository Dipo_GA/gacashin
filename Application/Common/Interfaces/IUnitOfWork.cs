using System.Threading.Tasks;

namespace Application.Common.Interfaces
{
    public interface IUnitOfWork
    {
        ICustomerInformation CustomerInformation { get; }
        Task CompleteAsync();
        void Dispose();
    }
}