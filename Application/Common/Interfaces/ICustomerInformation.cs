using Domain.Entities.Cellulant;

namespace Application.Common.Interfaces
{
    public interface ICustomerInformation : IGenericRepository<CustomerInformation>
    {
        
    }
}