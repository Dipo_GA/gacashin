using Application.Cellulant.Queries.DTOs;
using AutoMapper;
using Domain.Entities.Cellulant;

namespace Application.Common.Mappings
{
    public class DomainToResponseProfile : Profile
    {
        public DomainToResponseProfile()
        {
            CreateMap<CustomerInformation, CustomerInformationResponse>()
                .ForMember(dest => dest.AccountBalance, 
                    opt => opt.MapFrom(s => s.AccountBalanceInDec))
                .ForMember(dest => dest.KYCAmount, 
                    opt => opt.MapFrom(s => s.KYCAmountInDec));
        }
    }
}