using System.Threading;
using System.Threading.Tasks;
using Application.Cellulant.Queries.DTOs;
using MediatR;

namespace Application.Cellulant.Queries
{
    public class FetchBankListQuery : IRequest<BankListResponse>
    {
        public string SessionId { get; set; }
        public string PhoneNumber { get; set; }
    }

    public class FetchBankListQueryHandler : IRequestHandler<FetchBankListQuery, BankListResponse>
    {
        public FetchBankListQueryHandler()
        {
            
        }
        
        public async Task<BankListResponse> Handle(FetchBankListQuery request, CancellationToken cancellationToken)
        {
            return new BankListResponse();
        }
    }
}