using System.Threading;
using System.Threading.Tasks;
using Application.Cellulant.Queries.DTOs;
using Application.Common.Interfaces;
using AutoMapper;
using Domain.Entities.Cellulant;
using MediatR;

namespace Application.Cellulant.Queries
{
    public class FetchCustomerInformationQuery : IRequest<CustomerInformationResponse>
    {
        public string PhoneNumber { get; set; }
        public string Pin { get; set; }
    }

    public class FetchCustomerInformationQueryHandler : IRequestHandler<FetchCustomerInformationQuery, CustomerInformationResponse>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAPICalls<CustomerInformation> _apiCalls;
        private readonly IConfigurationManager _configurationManager;
        private readonly IURLManipulation _urlManipulation;
        private readonly IMapper _mapper;

        public FetchCustomerInformationQueryHandler(IUnitOfWork unitOfWork, IAPICalls<CustomerInformation> apiCalls, 
            IConfigurationManager configurationManager, IURLManipulation urlManipulation, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _apiCalls = apiCalls;
            _configurationManager = configurationManager;
            _urlManipulation = urlManipulation;
            _mapper = mapper;
        }
        public async Task<CustomerInformationResponse> Handle(FetchCustomerInformationQuery request, CancellationToken cancellationToken)
        {
            var url = $"{_configurationManager.RetrieveAppSettingsValue("baseUrl")}{_configurationManager.RetrieveAppSettingsValue("customerInfoUrl")}";
          
            var response = await _apiCalls.Get(_urlManipulation.ResolveCustomerInformationURL(request, url));

            var responseDto =  _mapper.Map<CustomerInformationResponse>(response);
            
            return ResolveReturnParams(responseDto);
        }

        private CustomerInformationResponse ResolveReturnParams(CustomerInformationResponse responseDto)
        {
            if (responseDto.ResponseCode == "00")
            {
                responseDto.ResponseDescription = "Customer Information retrieved successfully!";
            }
            else
            {
                responseDto.ResponseCode = "01";
                responseDto.ResponseDescription = "Unable to retrieve customer Information. Please try again";
            }

            return responseDto;
        }
    }
}