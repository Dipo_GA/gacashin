using System.Threading;
using System.Threading.Tasks;
using Application.Cellulant.Queries.DTOs;
using MediatR;

namespace Application.Cellulant.Queries
{
    public class FetchNameEnquiryQuery : IRequest<NameEnquiryResponse>
    {
        public string DestinationAccountNumber { get; set; }
        public string BankCode { get; set; }
        public string SessionId { get; set; }
    }

    public class FetchNameEnquiryQueryHandler : IRequestHandler<FetchNameEnquiryQuery, NameEnquiryResponse>
    {
        public FetchNameEnquiryQueryHandler()
        {
            
        }
        
        public async Task<NameEnquiryResponse> Handle(FetchNameEnquiryQuery request, CancellationToken cancellationToken)
        {
            return new NameEnquiryResponse();
        }
    }
}