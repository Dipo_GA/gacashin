namespace Application.Cellulant.Queries.DTOs
{
    public class NameEnquiryResponse : GenericResponse
    {   
        public string CustomerName { get; set; }
        public string EnquiryRefNo { get; set; }
    }
}