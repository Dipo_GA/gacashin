namespace Application.Cellulant.Queries.DTOs
{
    public class GenericResponse
    {
        public string ResponseCode { get; set; }
        public string ResponseDescription { get; set; }
    }
}