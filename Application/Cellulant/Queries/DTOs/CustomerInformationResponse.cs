namespace Application.Cellulant.Queries.DTOs
{
    public class CustomerInformationResponse : GenericResponse
    {
        public decimal AccountBalance { get; set; }
        
        public string Email { get; set; }
        public string Address { get; set; }
        public string Gender { get; set; }
        public string IsNew { get; set; }
        public string Occupation { get; set; }
        public string Type { get; set; }
        public string KYC { get; set; }
        public string Phone { get; set; }
        public string State { get; set; }
        public string FullName { get; set; }
        public decimal KYCAmount { get; set; }
        public string PlatformId { get; set; }


    }
}