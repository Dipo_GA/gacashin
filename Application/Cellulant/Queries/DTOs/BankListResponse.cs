using System.Collections.Generic;

namespace Application.Cellulant.Queries.DTOs
{
    public class BankInfo
    {
        public string BankCode { get; set; }
        public string BankName { get; set; }
    }

    public class BankListResponse : GenericResponse
    {
        public IEnumerable<BankInfo> BanksInformation { get; set; }
    }
}