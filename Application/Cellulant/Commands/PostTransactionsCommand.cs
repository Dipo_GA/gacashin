using System.Threading;
using System.Threading.Tasks;
using Application.Cellulant.Commands.DTOs;
using MediatR;

namespace Application.Cellulant.Commands
{
    public class PostTransactionsCommand : IRequest<PostTransactionResponse>
    {
        public string CellulantPin { get; set; }
        public string SourcePhoneNumber { get; set; }
        public string SourceEmail { get; set; }
        public string BeneficiaryPhoneNumber { get; set; }
        public string BeneficiaryLastNumber { get; set; }
        public string BeneficiaryOtherName { get; set; }
        
        public string BeneficiaryEmail { get; set; }
        public string BeneficiaryAccountNumber { get; set; }
        public string AccountType { get; set; }
        public string BankCode { get; set; }
        public string SessionId { get; set; }
        public string Narration { get; set; }
        public decimal Amount { get; set; }
        public string EnquiryRefNo { get; set; }
        public string PlatformId { get; set; }
    }

    public class PostTransactionsCommandHandler : IRequestHandler<PostTransactionsCommand, PostTransactionResponse>
    {
        public PostTransactionsCommandHandler()
        {
            
        }
        public async Task<PostTransactionResponse> Handle(PostTransactionsCommand request, CancellationToken cancellationToken)
        {
            return new PostTransactionResponse();
        }
    }
}