using System;
using Application.Cellulant.Queries.DTOs;

namespace Application.Cellulant.Commands.DTOs
{
    public class PostTransactionResponse : GenericResponse
    {
        public decimal Amount { get; set; }
        public string TransactionId { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}