using Domain.Common;

namespace Domain.Entities.Cellulant
{
    public class NameEnquiry : AuditableEntity
    {
        public string Name { get; set; }
        public string EnquiryRefNum { get; set; }
    }
}