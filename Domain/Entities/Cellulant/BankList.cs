using Domain.Common;

namespace Domain.Entities.Cellulant
{
    public class BankList : AuditableEntity
    {
        public string BankName { get; set; }
        public string BankCode { get; set; }
    }
}