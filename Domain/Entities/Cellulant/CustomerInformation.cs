using System;
using Domain.Common;

namespace Domain.Entities.Cellulant
{
    public class CustomerInformation : AuditableEntity
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public bool Status { get; set; }
        public string Address { get; set; }
        public string Gender { get; set; }
        public string IsNew { get; set; }
        public string Occupation { get; set; }
        public string Type { get; set; }
        public string KYC { get; set; }
        public string AccountBalance { get; set; }
        public string Phone { get; set; }
        public string State { get; set; }
        public string FullName { get; set; }
        public string KYCAmount { get; set; }
        public string PlatformId { get; set; }

        public Decimal AccountBalanceInDec => Convert.ToDecimal(AccountBalance);
        public Decimal KYCAmountInDec => Convert.ToDecimal(KYCAmount);
    }
}