using System;

namespace Domain.Common
{
    public abstract class AuditableEntity
    {
        public DateTime DateInserted => DateTime.Now;
        public string SessionId { get; set; }
        
        public string ResponseCode { get; set; }
        public bool Status { get; set; }
    }
}