using System;
using Application.Common.Interfaces;
using Infrastructure.Data;
using Infrastructure.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            var connString = configuration.GetConnectionString("GAMiddleWareConn");
            services.AddDbContextPool<AppDbContext>(options => options.UseMySql(connString, ServerVersion.AutoDetect(connString)));

            services.AddScoped(typeof(IAPICalls<>), typeof(CustomAPiCalls<>));
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IURLManipulation, URLManipulation>();
            
            services.AddHttpClient("CellulantBase", httpClient =>
            {
                httpClient.BaseAddress = new Uri(configuration["CellulantBaseUrl"]);
                
                httpClient.DefaultRequestHeaders.Add(
                    "Accept", "application/json");
                httpClient.DefaultRequestHeaders.Add(
                    "UserAgent", "GlobalAccelerex");
            });
            
            return services;
        }
    }
}