using Application.Cellulant.Queries;
using Application.Common.Interfaces;

namespace Infrastructure.Services
{
    public class URLManipulation: IURLManipulation
    {
        public string ResolveCustomerInformationURL(FetchCustomerInformationQuery reqObj, string url)
        {
            return url.Replace("##1", reqObj.PhoneNumber).Replace("##2", reqObj.Pin);
        }
    }
}