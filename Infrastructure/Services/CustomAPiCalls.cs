using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Application.Common.Interfaces;


namespace Infrastructure.Services
{
    public class CustomAPiCalls<T> : IAPICalls<T> where T: class
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public CustomAPiCalls(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        
        public async Task<T?> Get(string url)
        {
            T? result = null;
            
            var httpClient = _httpClientFactory.CreateClient("CellulantBase");
            var httpResponseMessage = await httpClient.GetAsync(url);

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                await using var contentStream =
                    await httpResponseMessage.Content.ReadAsStreamAsync();
            
                result = (T?) await JsonSerializer.DeserializeAsync
                    <T>(contentStream);
            }

            return result;
        }
        
        public async Task<IEnumerable<T>?> GetAll(string url)
        {
            IEnumerable<T>? result = null;
            
            var httpClient = _httpClientFactory.CreateClient("CellulantBase");
            var httpResponseMessage = await httpClient.GetAsync(url);

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                await using var contentStream =
                    await httpResponseMessage.Content.ReadAsStreamAsync();
            
                result = await JsonSerializer.DeserializeAsync
                    <IEnumerable<T>>(contentStream).ConfigureAwait(false);
            }

            return result;
        }

        public async Task<string> GetString(string url)
        {
            string result = null;
            
            var httpClient = _httpClientFactory.CreateClient("CellulantBase");
            var httpResponseMessage = await httpClient.GetAsync(url);

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                result = await httpResponseMessage.Content.ReadAsStringAsync();
            }

            return result;
        }

        public Task<T> QueryPost(string url)
        {
            throw new System.NotImplementedException();
        }

        public Task<T?> BodyPost(string url, T postObject)
        {
            throw new System.NotImplementedException();
        }
    }
}