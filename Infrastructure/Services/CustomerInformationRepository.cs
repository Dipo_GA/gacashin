using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using Domain.Entities.Cellulant;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Infrastructure.Services
{
    public class CustomerInformationRepository : GenericRepository<CustomerInformation>, ICustomerInformation
    {
        public CustomerInformationRepository(AppDbContext _context, ILogger logger): base(_context, logger)
        {
        }
        
        public override async Task<IEnumerable<CustomerInformation>> All()
        {
            try
            {
                return await _dbSet.ToListAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "{Repo} All function error", typeof(CustomerInformationRepository));
                return new List<CustomerInformation>();
            }
        }

        public Task<CustomerInformation> GetById(int id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Add(CustomerInformation entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Upsert(CustomerInformation entity)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<CustomerInformation>> Find(Expression<Func<CustomerInformation, bool>> predicate)
        {
            throw new NotImplementedException();
        }
    }
}