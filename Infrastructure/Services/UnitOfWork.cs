using System;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using Infrastructure.Data;
using Microsoft.Extensions.Logging;

namespace Infrastructure.Services
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly AppDbContext _context;
        private readonly ILoggerFactory _loggerFactory;

        public UnitOfWork(AppDbContext _context, ILoggerFactory loggerFactory)
        {
            this._context = _context;
            _loggerFactory = loggerFactory;
            CustomerInformation = new CustomerInformationRepository(_context, loggerFactory.CreateLogger("logs"));
        }
        public ICustomerInformation CustomerInformation { get; }
        
        public async Task CompleteAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}