using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using Newtonsoft.Json;

namespace Infrastructure.Services
{
    public class APICalls<T>: IAPICalls<T> where T: class
    {
        public async Task<T?> Get(string url)
        {
            T? result = null;
            using (var httpClient = new HttpClient())
            {
                var response = httpClient.GetAsync(new Uri(url)).Result;

                response.EnsureSuccessStatusCode();
                await response.Content.ReadAsStringAsync().ContinueWith((Task<string> x) =>
                {
                    if (x.IsFaulted)
                        throw x.Exception;

                    result = JsonConvert.DeserializeObject<T>(x.Result);
                });
            }

            return result;
        }

        public Task<IEnumerable<T>?> GetAll(string url)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetString(string url)
        {
            throw new NotImplementedException();
        }

        public Task<T> QueryPost(string url)
        {
            throw new System.NotImplementedException();
        }

        public async Task<T> BodyPost(string url, T postObject)
        {
            T? result = null;
            //
            // using (var client = new HttpClient())
            // {
            //     var response = await client.PostAsync(url, postObject, new JsonMediaTypeFormatter()).ConfigureAwait(false);
            //
            //     response.EnsureSuccessStatusCode();
            //
            //     await response.Content.ReadAsStringAsync().ContinueWith((Task<string> x) =>
            //     {
            //         if (x.IsFaulted)
            //             throw x.Exception;
            //
            //         result = JsonConvert.DeserializeObject<T>(x.Result);
            //
            //     });
            // }
            //
             return result;
        }
    }
}